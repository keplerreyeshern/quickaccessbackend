<!DOCTYPE html>
<html xml:lang="es">
    <head>
        <title>Acceso</title>
        <style>
            .btn {
                display: inline-block;
                font-weight: 400;
                color: #212529;
                text-align: center;
                vertical-align: middle;
                cursor: pointer;
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
                background-color: transparent;
                border: 1px solid transparent;
                padding: 0.375rem 0.75rem;
                font-size: 1rem;
                line-height: 1.5;
                border-radius: 0.25rem;
                transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
            }

            .btn-primary {
                color: #fff;
                background-color: #007bff;
                border-color: #007bff;
            }

            .btn-primary:hover {
                color: #fff;
                background-color: #0069d9;
                border-color: #0062cc;
            }
        </style>
    </head>
    <body>
        <p>
            Hola {{$name}}, la administración te esta dando acceso a QuickAccess. Tus credenciales de acceso son:<br>
            Usuario: {{$email}}<br>
            Contraseña: {{$password}}<br>
            Te sugerimos seguir con el proceso de registro entrando al enlace de abajo, en caso de necesitar registrar
            mas usuarios dar click en el enlace de registro e ingresar el siguiente codigo:
            <strong>{{$token}}</strong>
        </p>

        <a href="{{$login}}" class="btn btn-primary">Entrar</a>

        <a href="{{$register}}" class="btn btn-primary">Registrate</a>
        <br><br><br>
        <a href="{{$login}}">{{$login}}</a>
        <br>
        <a href="{{$register}}">{{$register}}</a>
    </body>
</html>
