<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserCreated extends Mailable
{
    use Queueable, SerializesModels;
    public $subject = 'Acceso';
    public $name;
    public $email;
    public $password;
    public $login;
    public $token;
    public $register;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $email, $password, $login, $register, $token)
    {
        $this->name = $name;
        $this->email = $email;
        $this->password = $password;
        $this->login = $login;
        $this->register = $register;
        $this->token = $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.user_created');
    }
}
