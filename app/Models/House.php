<?php

namespace App\models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class House extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'houses';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'street', 'deleted_at', 'company_id', 'complex_id', 'number'
    ];

    public function users()
    {
        return $this->hasMany('App\Models\User');
    }

    public function complex()
    {
        return $this->belongsTo('App\Models\Complex');
    }
}
