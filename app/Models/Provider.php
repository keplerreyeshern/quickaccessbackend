<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Provider extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'providers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'telephone',
        'status',
        'event_id',
        'company_id',
        'complex_id',
        'house_id'
    ];

    public function event()
    {
        return $this->belongsTo('App\Models\Event');
    }
    public function company()
    {
        return $this->belongsTo('App\Models\Company');
    }
    public function complex()
    {
        return $this->belongsTo('App\Models\Complex');
    }
    public function house()
    {
        return $this->belongsTo('App\Models\House');
    }

    public function routeNotificationForWhatsApp()
    {
        return $this->telephone;
    }
}
