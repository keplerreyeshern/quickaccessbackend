<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LocationData extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'location_data';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'house_id', 'company_id', 'complex_id', 'number', 'street', 'mzn', 'lt'
    ];
}
