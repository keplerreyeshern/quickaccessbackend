<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Complex extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'complexes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'status', 'deleted_at', 'company_id'
    ];

    public function users()
    {
        return $this->hasMany('App\Models\User');
    }

    public function houses()
    {
        return $this->hasMany('App\Models\House');
    }
}
