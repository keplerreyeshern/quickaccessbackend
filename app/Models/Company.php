<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'companies';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'status', 'token', 'guard_houses', 'logo'
    ];

    public function data(){
        return $this->hasOne('App\Models\EnterpriseData');
    }

    public function users(){
        return $this->hasMany('App\Models\User');
    }
}
