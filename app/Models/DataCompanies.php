<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DataCompanies extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'data_companies';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id', 'street', 'number', 'suburb', 'state', 'postal_code'
    ];
}
