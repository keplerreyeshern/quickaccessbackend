<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'events';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'status',
        'user_id',
        'dateStart',
        'dateEnd',
        'comments',
        'company_id',
        'complex_id',
        'house_id'
    ];

    public function inviteds()
    {
        return $this->hasMany('App\Models\Invited');
    }

    public function company(){
        return $this->belongsTo('App\Models\Company');
    }

    public function house(){
        return $this->belongsTo('App\Models\House');
    }

    public function complex(){
        return $this->belongsTo('App\Models\Complex');
    }
}
