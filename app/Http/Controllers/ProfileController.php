<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Complex;
use App\Models\DataCompanies;
use App\Models\House;
use App\Models\LocationData;
use App\Models\User;
use Illuminate\Http\Request;

class ProfileController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $user = auth()->user();
        $location = [];
        $house = '';
        $complex = '';
        if ($user->profile == 'user'){
            $location = LocationData::where('house_id', $user->house_id)->first();
            $complex = Complex::find($user->complex_id);
            $house = House::find($user->house_id);
        } else if ($user->profile == 'admin'){
            $location = DataCompanies::where('company_id', $user->company_id)->first();
        }
        $company = Company::findOrFail($user->company_id);
        $complexes = Complex::where('company_id', $user->company_id)->get();
        $houses = House::where('company_id', $user->company_id)->get();

        $userData = [
            'user' => $user,
            'company' => $company,
            'location' => $location,
            'complexes' => $complexes,
            'houses' => $houses,
            'complex' => $complex,
            'house' => $house,
        ];

        return json_encode($userData);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user = auth()->user();
        $user->name = $request['name'];
        $user->save();
        $verify = DataCompanies::where('company_id', auth()->user()->company_id)->get();
        if($verify){
            $location = DataCompanies::where('company_id', auth()->user()->company_id)->first();
            $location->street = $request['street'];
            $location->number = $request['number'];
            $location->state = $request['state'];
            $location->suburb = $request['suburb'];
            $location->postal_code = $request['postal_code'];
            $location->save();
        } else {
            $location = DataCompanies::create([
                'company_id' => auth()->user()->company_id,
                'street' => $request['street'],
                'number' => $request['number'],
                'state' => $request['state'],
                'suburb' => $request['suburb'],
                'postal_code' => $request['postal_code'],
            ]);
        }

        $result=[
            'location' => $location,
            'user' => $user,
        ];

        return json_encode($result);
    }

}
