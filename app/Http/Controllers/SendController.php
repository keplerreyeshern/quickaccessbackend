<?php

namespace App\Http\Controllers;

use App\Mail\UserCreated;
use App\Models\Code;
use App\Models\Event;
use App\models\House;
use App\Models\Invited;
use App\Models\User;
use App\Notifications\OrderProcessed;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Twilio\Rest\Client;

class SendController extends Controller
{
    public function index(Request $request)
    {
        $name = "Enrique Cordera";
        $code = "Kepler Reyes";
        $event = "Navidad";
        $date = "20/12/24";
        $coded = "56489";
        $twilio = new Client('AC10e9be5e3f0f06a04ed23d853756adc6', '81486986dc33434bf59217a7017f06f6');

        return $twilio->messages
        ->create("whatsapp:+5215613707070",
        [
            "mediaUrl" => ["https://backend.quickaccess.mx/storage/images/qrcodes/invitations/Olivos/Nubes/Reyes/Navidad/Kepler.png"],
        "from" => "whatsapp:+5215570055269",
        "body" => "Hola ".$name.", ".$code."  te envió el código QR para que puedas ingresar al evento ".$event.", con una fecha ".$date.". ¡Te estamos esperando! "
        ]);
//        return view('welcome');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $event = Event::find(1);

        $user = User::find(1);

        $user->notify(new OrderProcessed($event));


//        return redirect()->route('home')->with('status', 'Order Placed!');
    }
}
