<?php

namespace App\Http\Controllers;

use App\Models\Complex;
use App\Models\House;
use App\Models\User;
use Illuminate\Http\Request;

class ComplexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $complexes = Complex::where('company_id', auth()->user()->company_id)->get();
        $users = User::where('company_id', auth()->user()->company_id)->get();
        $houses = House::where('company_id', auth()->user()->company_id)->get();

        $result = [
            'complexes' => $complexes,
            'users' => $users,
            'houses' => $houses,
        ];

        return json_encode($result);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function active(Request $request)
    {
        $complex = Complex::find($request['id']);

        if ($request['status']){
            $complex->status = false;
        } else {
            $complex->status = true;
        }

        $complex->save();

        return json_encode($complex);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $complex = Complex::create([
            'name' => $request['name'],
            'company_id' => auth()->user()->company_id,
        ]);

        return json_encode($complex);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $complex = Complex::findOrFail($id);
        return json_encode($complex);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $complex = Complex::findOrFail($id);


        $complex->name = $request['name'];

        $complex->save();

        return json_encode($complex);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $complex = Complex::findOrFail($id);
        $complex->delete();
        return json_encode($complex);
    }
}
