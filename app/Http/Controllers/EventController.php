<?php

namespace App\Http\Controllers;

use App\Models\Complex;
use App\Models\Event;
use App\Models\House;
use App\Models\Invited;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Twilio\Rest\Client;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::where('company_id', auth()->user()->company_id)->get();
        $complexes = Complex::where('company_id', auth()->user()->company_id)->get();
        $users = User::where('company_id', auth()->user()->company_id)->get();
        $houses = House::where('company_id', auth()->user()->company_id)->get();
        $inviteds = Invited::where('company_id', auth()->user()->company_id)->get();

        $result = [
            'events' => $events,
            'complexes' => $complexes,
            'users' => $users,
            'houses' => $houses,
            'inviteds' => $inviteds
        ];

        return json_encode($result);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $lasts = Event::whereDate('dateEnd', '<=', date('Y-m-d').' 00:00:00')
            ->where('status', true)
            ->get();
        foreach ($lasts as $last){
            $last->status = false;
            $last->save();
        }
        $eventInActive = Event::where('user_id', auth()->user()->id)
            ->where('status', false)
            ->get();
        $eventActive = Event::where('user_id', auth()->user()->id)
            ->where('status', true)
            ->get();
        $events =[
            'eventsActive' => $eventActive,
            'eventsDisable' => $eventInActive
        ];
        return json_encode($events);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $name = $request['name'];
        $dateStart = date('Y-m-d H:i:s', strtotime(substr(str_replace('T', ' ', $request['dateStart']), 0, 19)));
        $dateEnd = date('Y-m-d H:i:s', strtotime(substr(str_replace('T', ' ', $request['dateEnd']), 0, 19)));
        $comment = $request['comments'];
        $user = auth()->user();
        $company = $user->company->name;
        if($user->complex_id){
            $complex = $user->complex->name.'/';
        } else {
            $complex = '';
        }
        $house = $user->house->name;

        $event = new Event();
        $event->user_id = $user->id;
        $event->house_id = $user->house_id;
        $event->complex_id = $user->complex_id;
        $event->company_id = $user->company_id;
        $event->name = $name;
        $event->dateStart = $dateStart;
        $event->dateEnd = $dateEnd;
        $event->comments = $comment;
        $event->status = true;
        $event->save();

        $inviteds = Invited::whereNull('event_id')->where('user_id', $user->id)->get();

        $twilio = new Client('AC10e9be5e3f0f06a04ed23d853756adc6', '81486986dc33434bf59217a7017f06f6');

        foreach ($inviteds as $invited){
            $coded = rand(5, 15);
            $code = QrCode::generate('Invited-'.$invited->id);
            Storage::disk('public')->put('/images/qrcodes/invitations/' . $company.'/'.$complex.
                ''.$house.'/' .$event->name.'/'.str_replace(' ', '', $invited->name).'.svg', $code);
            $twilio->messages
                ->create("whatsapp:+5215613707070",
                    [
                        "mediaUrl" => ["https://backend.quickaccess.mx/storage/images/qrcodes/invitations/". $company.'/'.$complex.''.$house.'/'.$event->name.'/'.str_replace(' ', '', $invited->name).'.svg'],
                        "from" => "whatsapp:+5215570055269",
                        "body" => "Hola ".$invited->name.", ".$user->name."  te envió el código QR para que puedas ingresar al evento ".$event.", con una fecha ".$dateStart.". ¡Te estamos esperando! ".$coded
                    ]);
            $invited->event_id = $event->id;
            $invited->company_id = $user->company_id;
            $invited->complex_id = $user->complex_id;
            $invited->house_id = $user->house_id;
            $invited->save();
        }

        $result = [
            'user' => $user,
            'name' => $name,
            'dateStart' => $dateStart,
            'dateEnd' => $dateEnd,
            'comment' => $comment,
            'message' => 'Se agregó el evento y se enviaron invitaciones con éxito'
        ];

        return json_encode($result);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $event = Event::find($id);
        $inviteds = Invited::where('event_id', $event->id)->get();
        $house = House::find($event->house_id);
        $complex = Complex::find($event->complex_id);

        $result = [
            'event' => $event,
            'inviteds' => $inviteds,
            'complex' => $complex,
            'house' => $house,
        ];

        return json_encode($result);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event = Event::where('id', $id)->first();
        $inviteds = Invited::where('event_id', $id)->get();
        $items = [
            'event' => $event,
            'inviteds' => $inviteds,
        ];
        return json_encode($items);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
