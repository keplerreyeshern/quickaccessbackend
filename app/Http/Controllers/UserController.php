<?php

namespace App\Http\Controllers;

use App\Mail\UserCreated;
use App\Models\Code;
use App\Models\Company;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\House;
use App\Models\Complex;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class UserController extends Controller
{
    public function index()
    {
        $users = User::where('company_id', auth()->user()->company_id)->get();
        $usersAdmin = User::where('profile', 'admin')->get();
        $houses = House::where('company_id', auth()->user()->company_id)->get();
        $complexes = Complex::where('company_id', auth()->user()->company_id)->get();
        $companies = Company::get();
        $data = [
            'users' => $users,
            'usersAdmin' => $usersAdmin,
            'houses' => $houses,
            'complexes' => $complexes,
            'companies' => $companies,
        ];
        return json_encode($data);
    }

    public function active(Request $request)
    {
        $user = User::find($request['id']);
        $company = Company::find($user->company_id);
        $complex = Complex::find($user->complex_id);
        $house = House::find($user->house_id);
        if ($complex){
            $complex = $complex->name.'/';
        }
        if ($user->profile == 'user'){
            $company = $company->name;
            $house = $house->name;
        }
        if ($user->status == 1){
            $user->status = 0;
        } else if ($user->status == 2){
            $user->status = 1;
            if($user->profile == 'user'){
                $code = QrCode::generate('Settler-'.$user->id);
                Storage::disk('public')->put('/images/qrcodes/' . $company.'/'.$complex.
                    ''.$house.'/'.str_replace(' ', '', $user->name).'.svg', $code);
            }

        } else {
            $user->status = 1;
        }
        $user->save();
        return json_encode($user);
    }

    public function changePassword($id, Request $request)
    {
        $user = User::find($id);
        if($user->status == 3){
            $user->status = 2;
        }
        $user->password = Hash::make($request['password']);
        $user->password_active = true;
        $user->save();
        return json_encode($user);
    }

    /**
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $login = 'https://quickaccess.mx/login';
        $register = 'https://quickaccess.mx/register';
        $token = Str::random(16);
        $to = $request['email'];
        $password = Str::random(8);
        $name = $request['name'];
        $email = $request['email'];
        if ($request['complex']){
            $complex = $request['complex'];
        } else {
            $complex = null;
        }
        if ($request['street']){
            $street = $request['street'];
        } else {
            $street = null;
        }
        if ($request['number']){
            $number = $request['number'];
        } else {
            $number = null;
        }
        $house = House::create([
            'name' => $request['house'],
            'street' => $street,
            'number' => $number,
            'complex_id' => $complex,
            'company_id' => auth()->user()->company_id,
            'status' => true,
        ]);
        $code = Code::create([
            'code' => $token,
            'company_id' => auth()->user()->company_id,
            'house_id' => $house->id,
            'complex_id' => $complex,
        ]);
        $user = User::create([
            'name' => $request['name'],
            'company_id' => auth()->user()->company_id,
            'profile' => 'user',
            'status' => 3,
            'house_id' => $house->id,
            'complex_id' => $complex,
            'telephone' => $request['telephone'],
            'email' => $request['email'],
            'password' => Hash::make($password),
        ]);

        Mail::to($to)->send(new UserCreated($name, $email, $password, $login, $register, $token));

        return json_encode($user);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $code = Code::firstWhere('code', $request['code']);
        if ($code->records < 6){
            $records = $code->records;
            $records = $records + 1;
            $code->records = $records;
            $code->save();
            $user = User::create([
                'name' => $request['name'],
                'company_id' => $code->company_id,
                'profile' => 'user',
                'status' => 2,
                'house_id' => $code->house_id,
                'complex_id' => $code->complex_id,
                'telephone' => $request['telephone'],
                'email' => $request['email'],
                'password' => Hash::make($request['password']),
            ]);
            $result = [
                'user' => $user,
                'code' => $code,
                'status' => true,
            ];
        } else {
            $result = [
                'status' => false,
                'user' => 'sin usuario',
                'code' => 'sin codigo',
            ];
        }

        return json_encode($result);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::create([
            'name' => $request['name'],
            'company_id' => auth()->user()->company_id,
            'profile' => 'vigilant',
            'active' => 1,
            'telephone' => $request['telephone'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
            'tollbooth' => $request['guardhouse'],
        ]);

        return json_encode($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return json_encode($user);
    }

    /**
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function confirm()
    {
        $users = User::get();

        return json_encode($users);
    }
}
