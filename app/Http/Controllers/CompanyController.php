<?php

namespace App\Http\Controllers;

use App\Mail\AdminCreated;
use App\Models\Company;
use App\Models\Complex;
use App\Models\DataCompanies;
use App\Models\Event;
use App\Models\House;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Company::get();
        $events = Event::get();
        $complexes = Complex::get();
        $users = User::get();
        $houses = House::get();

        $result = [
            'companies' => $companies,
            'events' => $events,
            'complexes' => $complexes,
            'users' => $users,
            'houses' => $houses
        ];

        return json_encode($result);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file = $request->file('logo');
        $login = 'https://quickaccess.mx/login';
        $token = Str::random(16);
        $to = $request['email'];
        $password = Str::random(8);
        $company = new Company();
        if ($file){
            $name =  time()."_".$file->getClientOriginalName();
            Storage::disk('public')->put('/images/logos/' . $name,  \File::get($file));
            $company->logo = '/logos/' . $name;
        }
        $company->name = $request['name'];
        $company->token = $token;
        $company->save();

        $user = new User();
        $user->name =  $request['contact'];
        $user->email =  $request['email'];
        $user->telephone =  $request['telephone'];
        $user->password = Hash::make($password);
        $user->profile = 'admin';
        $user->company_id = $company->id;
        $user->save();


        $body = 'Hola '.$request['name'] . ', ya cuentas con acceso a QuickAccess.
        Tus credenciales de acceso son:'. "\n".'Usuario: '. $request['email']. "\n".'Contraseña: '.$password. "\n".
        'Te sugerimos guardar el siguiente codigo: <strong>'.$token.'</strong> ya que es el token de seguridad de tu
        empresa o residencial';

        Mail::to($to)->send(new AdminCreated($body, $login, $token));

        return json_encode($company);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $company = Company::find($id);
        if ($company->status){
            $company->status = false;
        } else {
            $company->status = true;
        }
        $company->save();
        return json_encode($company);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $company = Company::find($id);
        $company->name = $request['name'];
        $company->save();
        return json_encode($company);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $company = Company::find($id);
        $company->delete();
        return json_encode($company);
    }
}
