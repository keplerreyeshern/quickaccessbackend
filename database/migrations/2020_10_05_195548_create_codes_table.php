<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('codes', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->integer('status')->default(true);
            $table->foreignId('company_id')->nullable()
                ->references('id')
                ->on('companies')
                ->onDelete('cascade');
            $table->foreignId('complex_id')->nullable()
                ->references('id')
                ->on('complexes')
                ->onDelete('cascade');
            $table->foreignId('house_id')->nullable()
                ->references('id')
                ->on('houses')
                ->onDelete('cascade');
            $table->integer('records')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('codes');
    }
}
