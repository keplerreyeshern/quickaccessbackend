<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->foreignId('house_id')
                ->references('id')
                ->on('houses')
                ->onDelete('cascade');
            $table->foreignId('complex_id')->nullable()
                ->references('id')
                ->on('complexes')
                ->onDelete('cascade');
            $table->foreignId('company_id')
                ->references('id')
                ->on('companies')
                ->onDelete('cascade');
            $table->string('name');
            $table->dateTime('dateStart');
            $table->dateTime('dateEnd');
            $table->text('comments')->nullable();
            $table->boolean('status');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
