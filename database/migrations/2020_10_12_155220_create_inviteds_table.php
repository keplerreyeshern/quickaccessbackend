<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvitedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inviteds', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->nullable();
            $table->string('telephone');
            $table->integer('status')->default(1);
            $table->foreignId('event_id')->nullable()
                ->references('id')
                ->on('events')
                ->onDelete('cascade');
            $table->foreignId('company_id')
                ->nullable()
                ->references('id')
                ->on('companies')
                ->onDelete('cascade');
            $table->foreignId('complex_id')->nullable()
                ->references('id')
                ->on('complexes')
                ->onDelete('cascade');
            $table->foreignId('house_id')->nullable()
                ->references('id')
                ->on('houses')
                ->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inviteds');
    }
}
