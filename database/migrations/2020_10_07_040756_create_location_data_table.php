<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocationDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('location_data', function (Blueprint $table) {
            $table->id();
            $table->foreignId('house_id')
                ->nullable()
                ->references('id')
                ->on('houses')
                ->onDelete('cascade');
            $table->foreignId('complex_id')
                ->nullable()
                ->references('id')
                ->on('complexes')
                ->onDelete('cascade');
            $table->foreignId('company_id')
                ->nullable()
                ->references('id')
                ->on('companies')
                ->onDelete('cascade');
            $table->string('street')->nullable();
            $table->string('number')->nullable();
            $table->string('mzn')->nullable();
            $table->string('lt')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('location_data');
    }
}
