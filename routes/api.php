<?php

use App\Http\Controllers\ComplexController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\EventController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\ProviderController;
use App\Http\Controllers\InvitedController;
use App\Models\Company;
use App\Models\Complex;
use App\Models\DataCompanies;
use App\models\House;
use App\Models\LocationData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    $user = auth()->user();
    $location = [];
    $house = '';
    $complex = '';
    if ($user->profile == 'user'){
        $location = LocationData::where('house_id', $user->house_id)->first();
        $complex = Complex::find($user->complex_id);
        $house = House::find($user->house_id);
    } else if ($user->profile == 'admin'){
        $location = DataCompanies::where('company_id', $user->company_id)->first();
    }
    $company = Company::findOrFail($user->company_id);
    $complexes = Complex::where('company_id', $user->company_id)->get();
    $houses = House::where('company_id', $user->company_id)->get();

    $userData = [
        'user' => $user,
        'company' => $company,
        'location' => $location,
        'complexes' => $complexes,
        'houses' => $houses,
        'complex' => $complex,
        'house' => $house,
    ];

    return json_encode($userData);
});

Route::post('/users/register', [UserController::class, 'register']);
Route::post('/users/confirm', [UserController::class, 'confirm']);

Route::group(['middleware' => ['auth:api']], function () {
    Route::get('/profile', [ProfileController::class, 'index']);
    Route::post('/profile/update', [ProfileController::class, 'update']);
    Route::resource('/users', UserController::class)->except([
        'show', 'create', 'edit'
        ]);
    Route::post('/users/active', [UserController::class, 'active']);
    Route::post('/users/change/password/{id}', [UserController::class, 'changePassword']);
    Route::post('/users/save', [UserController::class, 'save']);
    Route::resource('/complexes', ComplexController::class)->except(['create', 'edit']);
    Route::post('/complexes/active', [ComplexController::class, 'active']);
    Route::resource('/events', EventController::class)->except([
        'update', 'destroy'
    ]);
    Route::resource('/companies', CompanyController::class)->except([
        'create', 'edit'
    ]);
    Route::resource('/providers', ProviderController::class)->except([
        'create', 'edit', 'show', 'update', 'destroy'
    ]);
    Route::resource('/inviteds', InvitedController::class)->except([
        'create', 'edit', 'show', 'update', 'destroy'
    ]);
});
